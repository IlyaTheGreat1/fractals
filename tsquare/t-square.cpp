#include <GL/glut.h>
#include <stdlib.h>
#include <iostream>

void drawSquare(int xCenter, int yCenter, int length, int iterator);

void init()
{
    /**
     * @brief Палитра:
     *      0.0, 0.5, 1.0, 0.0  - голубой
     *      1.0, 0.5, 1.0, 0.0  - поняшно-розовый
     */
    glClearColor(1.0, 0.5, 0.0, 0.0);
    /**
     * @brief Construct a new gl Matrix Mode object
     * Конструкия представляет окно в виде матрицы
     */
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(-400, 400, -400, 400); //Точка начала координат в центре окна 800х800
}

void drawSquare(int xCenter, int yCenter, int length, int iterator)
{
    /**
     * @brief вычисление координат углов квадрата
     * используя координаты центра
     */
    int x1 = xCenter - length / 2;
    int x2 = xCenter + length / 2;
    int y1 = yCenter - length / 2;
    int y2 = yCenter + length / 2;

    glVertex2i(x1, y1); //Описывает геометрию отрезка 
    glVertex2i(x1, y2);
    glVertex2i(x2, y2);
    glVertex2i(x2, y1);

    if (iterator) {
        drawSquare(x1, y1, length / 2, iterator - 1);
        drawSquare(x1, y2, length / 2, iterator - 1);
        drawSquare(x2, y2, length / 2, iterator - 1);
        drawSquare(x2, y1, length / 2, iterator - 1);
    }
}
 
void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(0.0, 0.0, 0.0); //Цвет кисти
    glBegin(GL_QUADS); //Отрисовка примитива, параметры задаются между glBegin и glEnd
    drawSquare(0, 0, 400, 10);
    glEnd();
    glFlush();
}
 
int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(800, 800);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("T-Square");
    init();
    glutDisplayFunc(display);
    glutMainLoop();
    return 0;
}